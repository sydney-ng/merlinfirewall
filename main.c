import requests
import json
import csv

def main_handler(region_name, service_name):
    r = requests.get('https://ip-ranges.amazonaws.com/ip-ranges.json')
    json_data = json.loads(r.text)
    valid_ips = []
    for i in json_data['prefixes']:
        if (i ['region'] == region_name) and (i['service'] == service_name):
            valid_ips.append(i['ip_prefix'])
    with open ('Merlin_IPs.csv', "w") as f:
        writer = csv.writer(f, delimiter=',')
        for ip in valid_ips:
            writer.writerow([ip])

def main ():
    main_handler('us-west-1', 'S3')
main()